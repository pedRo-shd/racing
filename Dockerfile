FROM ruby:2.4-slim

# Instala nossas dependencias
RUN apt-get update && apt-get install -qq -y --no-install-recommends \
      build-essential nodejs libpq-dev imagemagick

# Seta nosso path
ENV INSTALL_PATH /racing

# Cria nosso diretório
RUN mkdir -p $INSTALL_PATH

# Seta o nosso path como o diretório principal
WORKDIR $INSTALL_PATH

# Copia o nosso Gemfile para dentro do container
COPY Gemfile Gemfile.lock ./

# Seta o path para as Gems
RUN bundle install

# Copia nosso código para dentro do container
COPY . .

CMD puma -C config/puma.rb
