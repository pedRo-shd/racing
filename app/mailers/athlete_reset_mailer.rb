class AthleteResetMailer < ApplicationMailer
  default from: 'obikerapi@obiker.com'
  def pass_reset_athlete(athlete, password)
    @athlete = athlete
    @password = password
    mail(to: @athlete.email, subject: 'Obiker Reset Password')
  end
end
