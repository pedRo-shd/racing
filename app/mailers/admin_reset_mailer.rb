class AdminResetMailer < ApplicationMailer
  default from: 'obikerapi@obiker.com'
  def pass_reset_admin(admin, password)
    @admin = admin
    @password = password
    mail(to: @admin.email, subject: 'Obiker Reset Password')
  end
end
