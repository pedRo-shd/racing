class OrganizationResetMailer < ApplicationMailer
  default from: 'obikerapi@obiker.com'
  def pass_reset_organization(organization, password)
    @organization = organization
    @password = password
    mail(to: @organization.email, subject: 'Obiker Reset Password')
  end
end
