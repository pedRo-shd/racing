class EventContactsMailer < ApplicationMailer
  default from: 'obikerapi@obiker.com'

  def more_info_event(opt, organizer, event)
    @organization = organizer
    @event = event
    @user_name = opt.name
    @title = opt.title
    @description = opt.description

    mail(to: organizer_email.email, subject: 'Informações do evento')
  end
end
