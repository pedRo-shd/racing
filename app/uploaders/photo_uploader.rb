class PhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include Cloudinary::CarrierWave unless Rails.env.test?

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process resize_to_fit: [180, 180]
  end

  version :big_main_event do
    process resize_to_fill: [1080, 400]
  end

  version :profile_thumb, from_version: :thumb do
      process resize_to_fill: [220, 190]
  end

  version :event_thumb do
      process resize_to_fill: [180, 160]
  end

  version :event_home do
      process resize_to_fill: [525, 250]
  end

  version :event_show do
      process resize_to_fill: [920, 400]
  end

  def extension_whitelist
    %w(jpg jpeg gif png)
  end
end
