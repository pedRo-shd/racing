class DaysToGo

  def initialize(event_date, hours_event)
    @event_date = event_date
    @hours_event = hours_event
  end

  def the_day
    subtract_days = @event_date - Date.current
    day_to_start_event = subtract_days.to_i
    day_to_start_event.to_s
  end

  def result_to_go
    seconds_to_start = @hours_event.to_i - Time.current.to_i
    # seconds_to_start = - seconds_to_start if seconds_to_start.negative?

    time_to_start = Time.new(0) + seconds_to_start
    time_to_start
  end

  def the_hours
    result_to_go.hour.to_s
  end

  def the_minutes
    result_to_go.min.to_s
  end
end
