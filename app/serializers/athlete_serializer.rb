class AthleteSerializer < ActiveModel::Serializer
  attributes :id, :name, :cpf, :email, :birth_date, :gender, :avatar, :my_team
  has_many :dependents
  has_many :events
end
