class OrganizationSerializer < ActiveModel::Serializer
  attributes :name,
             :email,
             :street,
             :number,
             :neighbor,
             :city,
             :state,
             :cpf,
             :cnpj,
             :phone,
             :second_phone,
             :photo
  has_many :events
  has_many :memberships, through: :events
end
