# app/controllers/v1/dependents_controller.rb
module V1
  class DependentsController < ApplicationController
    skip_before_action :authorize_organization_request
    skip_before_action :authorize_admin_request

    before_action :set_dependent, only: %i[show update destroy member_team member_events join_event leave_event avatar_url create_invoice delete_invoice]

    # GET /athletes/:athlete_id/dependents
    def index
      @dependents = current_athlete.dependents
      json_response(@dependents)
    end

    # # GET /athletes/:athlete_id/dependents/:id
    def show
      json_response(@dependent)
    end

    # # POST /athletes/:athlete_id/dependents
    def create
      @athlete = current_athlete.dependents.create!(dependent_params)
      json_response(@athlete, :created)
    end

    # PUT /athletes/:athlete_id/dependents/:id
    def update
      @dependent.update(dependent_params)
      head :no_content
    end

    # # DELETE /athletes/:athlete_id/dependents/:id
    def destroy
      @dependent.destroy
      head :no_content
    end

    def member_team
      if @dependent.team
        @team = @dependent.team
        json_response(@team)
      else
        json_response("This member don't have joined a team")
      end
    end

    def member_events
      if @dependent.events.present?
        @events = @dependent.events
        json_response(@events)
      else
        json_response("This member don't have joined any events")
      end
    end

    def join_event
      @event = Event.find(params[:event_id])
      @dependent.events << @event
      @invoice = create_invoice(@event, @dependent.athlete)
      puts @invoice
      json_response("You join to the #{@event.name} event")
    end

    def leave_event
      @event = Event.find(params[:event_id])
      to_remove = @dependent.events.find(@event.id)
      @dependent.events.delete(to_remove)
      delete_invoice
      json_response("You leave the #{@event.name} event")
    end

    def load_teams
      @teams = Team.all
      json_response(@teams)
    end

    def load_events
      @events = Event.all
      json_response(@teams)
    end

    def avatar_url
      @dependent = Dependent.find(params[:id])
      url = @dependent.avatar.url(:default, timestamp: false)
      json_response(avatar_url: url)
    end

    private

    def delete_invoice
      @invoice = Invoice.where('athlete_id = ? and event_id = ? and dependent_id = ?', current_athlete.id, params[:event_id], @dependent.id)
      Invoice.delete(@invoice)
    end

    def create_invoice(event, athlete)
      @invoice = Invoice.create!(athlete_id: athlete.id,
                      event_id: event.id,
                      product_id: event.id,
                      product_desc: "Inscrição: #{event.name} - Dependente",
                      price: event.price,
                      quantity: 1,
                      customer_name: athlete.name,
                      dependent_id: @dependent.id,
                      customer_address: customer_address(athlete)
                      )
      adjust_price(@invoice, event)
    end

    def adjust_price(invoice, event)
      invoice.update_attributes(final_price: event.price)
    end

    def customer_address(athlete)
      "#{athlete.street},
      #{athlete.number},
      #{athlete.neighbor},
      #{athlete.city},
      #{athlete.state}"
    end

    def dependent_params
      params.permit(:name, :cpf, :birth_date, :gender, :team_id, :avatar)
    end

    def set_dependent
      @dependent = current_athlete.dependents.find_by!(id: params[:id])
    end
  end
end
