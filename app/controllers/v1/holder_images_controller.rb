# app/controllers/v1/holder_images_controller.rb
module V1
  class HolderImagesController < ApplicationController
    skip_before_action :authorize_athlete_request
    skip_before_action :authorize_admin_request
    
    before_action :set_event, only: %i[create destroy]

    
    def create
      @image = @event.holder_images.create!(image_holder_params)
      json_response(@image, :created)
    end

    
    def destroy
      @event.holder_images.find(params[id]).destroy
      head :no_content
    end


    private
    def image_holder_params
      params.permit(
        :event_id,
        :image
      )
    end
    

    def set_event
      @event = Event.find(params[:event_id])
    end
  end
end
