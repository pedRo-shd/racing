module V1
  require 'httparty'
  class CheckoutsController < ApplicationController
      skip_before_action :authorize_athlete_request, only: :response_update
      skip_before_action :authorize_admin_request
      skip_before_action :authorize_organization_request
      include HTTParty

      PagarMe.api_key        = ['pagarme_key']
      PagarMe.encryption_key = ['pagarme_encryption']

      def checkout_bill
        @invoice = Invoice.where('athlete_id = ? and event_id = ? and processed = ?', current_athlete.id, params[:event_id], false)
        if @invoice.present?
          transaction = PagarMe::Transaction.new(
                amount: (@invoice.first.final_price.to_f * 100).to_i,
                customer: {
                  external_id: current_athlete.id.to_s,
                  name: current_athlete.name,
                  type: "individual",
                  country: "br",
                  email: current_athlete.email,
                  documents: [
                    {
                      type: "cpf",
                      number: current_athlete.cpf ? current_athlete.cpf : "00000000000"
                    }
                  ],
                  birthday: current_athlete.birth_date.to_s
                },
                billing: {
                  name: current_athlete.name,
                  address: {
                    country: "br",
                    state: current_athlete.state,
                    city: current_athlete.city,
                    neighborhood: current_athlete.neighbor,
                    street: current_athlete.street,
                    street_number: current_athlete.number,
                    zipcode: current_athlete.zipcode
                  }
                },
                items: [
                  {
                    id: @invoice.first.product_id.to_s,
                    title: @invoice.first.product_desc,
                    unit_price: @invoice.first.price.to_i * 100,
                    quantity: "1",
                    tangible: false
                  }
                ],
                split_rules: [
                  {
                    percentage: @invoice.first.event.split,
                    recipient_id: @invoice.first.event.organization.recipent_id
                  },
                  {
                    percentage: (100 - @invoice.first.event.split),
                    recipient_id: Admin.last.recipent_id
                  }
                ],
                async: false,
                postback_url: "https://requestb.in/17u83m51",
                payment_method: 'boleto'
            )
            
            transaction.charge
            
            bill_udpate_membership(@invoice.first)
            processe_invoice(@invoice.first)

        else
          json_response(message: "You don't have any invoice to checkout")
        end
      end

      def checkout_card
        @athlete = current_athlete
        @invoice = Invoice.where('athlete_id = ? and event_id = ? and processed = ?', current_athlete.id, params[:event_id], false)
        @api_key = ['pagarme_key']
        @checkout_card_url = "https://api.pagar.me/1/transactions?api_key=#{@api_key}"

        if @invoice.present?
      
          @transaction = HTTParty.post(@checkout_card_url,
          body: { amount: (@invoice.first.final_price.to_f * 100).to_i,
            card_number: params[:card_number],
            card_cvv: params[:card_cvv],
            card_expiration_date: params[:card_expiration_date],
            card_holder_name: params[:card_holder_name],
            customer: {
              external_id: current_athlete.id.to_s,
              name: current_athlete.name,
              type: "individual",
              country: "br",
              email: current_athlete.email,
              documents: [
                {
                  type: "cpf",
                  number: current_athlete.cpf ? current_athlete.cpf : "00000000000"
                }
              ],
              phone_numbers: ["+55#{current_athlete.phone}"],
              birthday: current_athlete.birth_date.to_s
            },
            installments: params[:installments],
            billing: {
              name: current_athlete.name,
              address: {
                country: "br",
                state: current_athlete.state,
                city: current_athlete.city,
                neighborhood: current_athlete.neighbor,
                street: current_athlete.street,
                street_number: current_athlete.number,
                zipcode: current_athlete.zipcode
              }
            },
            items: [
              {
                id: @invoice.first.product_id.to_s,
                title: @invoice.first.product_desc,
                unit_price: (@invoice.first.price.to_f * 100).to_i,
                quantity: "1",
                tangible: false
              }
            ],
            split_rules: [
              {
                percentage: @invoice.first.event.split,
                recipient_id: @invoice.first.event.organization.recipent_id
              },
              {
                percentage: (100 - @invoice.first.event.split),
                recipient_id: Admin.last.recipent_id
              }
            ],
            async: false,
            postback_url: "https://requestb.in/17u83m51"
          }.to_json,

          headers: { 'Content-Type' => 'application/json' }
          )
          card_udpate_membership(@invoice.first, @transaction)
          processe_invoice(@invoice.first)
        else
          json_response(message: "You don't have any invoice to checkout")
        end
      end

      def response_update
        if transaction['payment_method'] == 'boleto'
          @invoice = Invoice.where('transaction_id = ?', transaction['tid'])
          if invoice.dependent_id.blank?
            @membership = Membership.where('athlete_id = ? and event_id = ?', @invoice.athlete_id, @invoice.event_id)
            if transaction['status'] == 'paid'
              @membership.first.update_attributes(approved: true, status: 'paid')
            elsif transaction['status'] == 'refused'
              @membership.first.update_attributes(approved: true, status: 'refused')
            end
          else
            @comembership = Comembership.where('athlete_id = ? and event_id = ?', @invoice.athlete_id, @invoice.event_id)
            if transaction['status'] == 'paid'
              @comembership.first.update_attributes(approved: true, status: 'paid')
            elsif transaction['status'] == 'refused'
              @comembership.first.update_attributes(approved: true, status: 'refused')
            end
          end
        end
      end

      private

      def processe_invoice(invoice)
        invoice.update_attributes(processed: true)
      end

      def card_udpate_membership(invoice, transaction)
        if invoice.dependent_id.blank?
          @membership = Membership.where('athlete_id = ? and event_id = ?', invoice.athlete_id, invoice.event_id)
          if transaction['status'] == 'paid'
            @membership.first.update_attributes(approved: true, status: 'paid')
          elsif transaction['status'] == 'refused'
            @membership.first.update_attributes(approved: true, status: 'refused')
          end
        else
        @comembership = Comembership.where('dependent_id = ? and event_id = ?', invoice.dependent_id, invoice.event_id)
          if transaction['status'] == 'paid'
            @comembership.first.update_attributes(approved: true, status: 'paid')
          elsif transaction['status'] == 'refused'
            @comembership.first.update_attributes(approved: true, status: 'refused')
          end
        end
      end

      def bill_udpate_membership(invoice)
        if invoice.dependent_id.blank?
          @membership = Membership.where('athlete_id = ? and event_id = ?', invoice.athlete_id, invoice.event_id)
          @membership.first.update_attributes(approved: true, status: 'waiting_payment')
        else
          @comembership = Comembership.where('dependent_id = ? and event_id = ?', invoice.dependent_id, invoice.event_id)
          @comembership.first.update_attributes(approved: true, status: 'waiting_payment')
        end
      end
  end
end