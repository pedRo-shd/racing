class V1::NextEventsController < ApplicationController

  skip_before_action :authorize_athlete_request
  skip_before_action :authorize_admin_request
  skip_before_action :authorize_organization_request

  def index
    day_today = Time.current
    seven_days_ago = Time.current + 15.days
    # Caso não esteja sendo selecionado por página, pegamos a primeira
    page = params[:page] || 1
    @next_events = Event.where(event_date: day_today..seven_days_ago).paginate(page: page, per_page: 18)

    json_response @next_events
  end
end
