# app/controllers/v1/events_controller.rb
module V1
  class EventsController < ApplicationController
    skip_before_action :authorize_athlete_request, except: [:join_event, :leave_event]
    skip_before_action :authorize_admin_request, only: %i[ index autocomplete
                                                           show
                                                           organization_event
                                                            hours_to_go
                                                            minutes_to_go
                                                            day_to_go
                                                            get_events_carousel
                                                            create
                                                          ]
    skip_before_action :authorize_organization_request, only: [ :join_event,
                                                                :leave_event,
                                                                :event_image_url,
                                                                :kit_image_url,
                                                                :holder_images,
                                                                :do_the_recall,
                                                                :autocomplete,
                                                                :show,
                                                                :organization_event,
                                                                :hours_to_go,
                                                                :minutes_to_go,
                                                                :day_to_go,
                                                                :get_events_carousel
                                                              ]

    before_action :set_event, only: %i[update destroy cancel_event recall_event]
    before_action :set_global_event, only: %i[ show
                                                event_split
                                                join_event
                                                leave_event
                                                event_image_url
                                                kit_image_url
                                                holder_images
                                                use_voucher
                                                delete_invoice
                                                update_membership
                                                organization_event
                                                hours_to_go
                                                minutes_to_go
                                                day_to_go
                                              ]

    # GET /organizations/:organization_id/events
    def index
      @events = current_organization.events
      json_response(@events)
    end

    # # GET /organizations/:organization_id/events/:id
    def show
      json_response(@g_event)
    end

    def organization_event
      @organization_event = @g_event.organization
      json_response(@organization_event)
    end

    # GET /api/v1/autocomplete.json
    def autocomplete
      results = []
      Event.where(status: :open).each do |event|
        results << event.name
        results << event.start_city
        results << event.start_state
        results << event.start_neighbor
      end
      render json: results, status: 200
    end

    def clone_event
      @cloned = Event.find(params[:id])
      json_response(@cloned)
    end

    # # POST /organizations/:organization_id/events
    def create
      @event_from_organization = current_organization.events.create!(event_params)
      json_response @event_from_organization
    end

    # PUT /organizations/:organization_id/events/:id
    def update
      @event.update(event_params)
      head :no_content
    end

    # # DELETE /organizations/:organization_id/events/:id
    def destroy
      @event.destroy
      head :no_content
    end

    def join_event
      if joined?
        json_response(message: 'You already joined this event')
      else
        if params[:voucher].present?
          @voucher = Voucher.find_by_number(params[:voucher])
          if used_voucher(@voucher)
            json_response(message: 'This voucher is alread used')
          else
            if use_voucher()
              current_athlete.events << @g_event
              update_membership()
              @invoice = create_invoice(@g_event, current_athlete)
              @invoice.update_attributes(discount: @voucher.discount)
              adjust_price(@invoice, @g_event)
              json_response(message: "You join to the #{@g_event.name} event")
            else
              json_response(message: 'Voucher number not found')
            end
          end
        else
          current_athlete.events << @g_event
          json_response(message: "You join to the #{@g_event.name} event")
          create_invoice(@g_event, current_athlete)
          adjust_price(@invoice, @g_event)
        end
      end
    end

    def leave_event
      to_remove = current_athlete.events.find(@g_event.id)
      current_athlete.events.delete(to_remove)
      delete_invoice()
      json_response(message: "You leave the #{@g_event.name} event")
    end

    def event_image_url
      url = @g_event.event_image.url(:default, timestamp: false)
      json_response(event_image_url: url)
    end

    def kit_image_url
      url = @g_event.kit_image.url(:default, timestamp: false)
      json_response(kit_image_url: url)
    end

    def holder_images
      @images = @g_event.holder_images.map{ |url| url.image.url(:default, timestamp: false) }
      json_response(holder_image_url: @images)
    end

    def cancel_event
      if current_organization
        if event_subs_rate >= 20
          json_response( message: "This event can't be canceled, because it have more than 20% of subscriptions.")
        else
          @event.update_attribute(:be_canceled, true)
          json_response(message: 'This event is waiting approve for the cancellation.')
        end
      else
        json_response(message: 'You need to be an organzation to cancel an event!')
      end
    end

    def recall_event
      if current_organization
        if @event.update_attributes(be_recalled: true,
                                    new_event_date: params[:new_event_date],
                                    recall_start: Date.current,
                                    recall_end: Date.current + 7)
          json_response(message: 'This event is waiting approve to be recalled.')
        else
          json_response(message: "This event can't be recaled. Contact the Administrator")
        end
      else
        json_response(message: 'You need to be an organzation to recall an event!')
      end
    end

    def do_the_recall
      @events = Event.all.where('recall_end = ?', Date.current)
      @events.each do |event|
        if event_recall_rate(event) > 80
          event.update_attributes(status: 'recalled', event_date: new_event_date)
        else
          event.update_attributes(status: 'canceled')
        end
      end
    end

    def day_to_go
      @days_to_go = DaysToGo.new(@g_event.event_date, nil).the_day
      json_response(@days_to_go)
    end

    def hours_to_go
      @hours_to_go = DaysToGo.new(nil, @g_event.start_hours).the_hours
      json_response(@hours_to_go)
    end

    def minutes_to_go
      @minutes_to_go = DaysToGo.new(nil, @g_event.start_hours).the_minutes
      json_response(@minutes_to_go)
    end

    def get_events_carousel
      @events_carousel = Event.three_first
      json_response(@events_carousel)
    end

    private
    def joined?
      if Membership.where('event_id = ? and athlete_id = ?', @g_event.id, current_athlete.id).count > 0
        true
      else
        false
      end
    end

    def delete_invoice
      @invoice = Invoice.where('athlete_id = ? and event_id = ?', current_athlete.id, @g_event)
      Invoice.delete(@invoice)
    end

    def create_invoice(event, athlete)

      @invoice = Invoice.create!(athlete_id: athlete.id,
                      event_id: event.id,
                      product_id: event.id,
                      product_desc: "Inscrição: #{event.name}",
                      price: event.price,
                      quantity: 1,
                      customer_name: athlete.name,
                      customer_address: customer_address(athlete)
                      )
    end

    def adjust_price(invoice, event)
      if invoice.discount > 0
        invoice.update_attributes(final_price: (event.price * invoice.discount / 100))
      else
        invoice.update_attributes(final_price: event.price)
      end
    end

    def customer_address(athlete)
      "#{athlete.street},
      #{athlete.number},
      #{athlete.neighbor},
      #{athlete.city},
      #{athlete.state}"
    end

    def used_voucher(voucher)
      if voucher.used == true
        true
      else
        false
      end
    end

    def use_voucher
      voucher = params[:voucher]
      if @voucher = Voucher.find_by_number(voucher)
        @voucher.update_attributes(used: true)
        true
      else
        false
      end
    end

    def update_membership
      membership = Membership.where('event_id = ? and athlete_id = ?', @g_event.id, current_athlete.id)
      membership.update_all(voucher: @voucher.number, discount: @voucher.discount)
    end

    def event_split
      if current_admin
        if @g_event.split.blank?
          @g_event.update_attributes(split: params[:split], status: 'open')
          json_response(message: "You set #{@g_event.split}% as a split rule for the event: #{@g_event.name}")
        else
          json_response(message: "The split for the event: #{@g_event.name} is already seted")
        end
      else
        json_response(message: 'Only admins can determine splite value')
      end
    end

    def event_params
      params.permit(
        :name,
        :start_street,
        :start_number,
        :start_neighbor,
        :start_city,
        :start_state,
        :finish_street,
        :finish_number,
        :finish_neighbor,
        :finish_city,
        :finish_state,
        :start_address,
        :start_hours,
        :finish_address,
        :start_lat,
        :start_long,
        :finish_start,
        :finish_long,
        :event_date,
        :event_info,
        :photo,
        :kit_description,
        :contact_email,
        :contact_phone,
        :status,
        :mode,
        :rules,
        :subscription_start,
        :subscription_end,
        :max_subscriptions,
        :modality_id,
        :price
      )
    end

    def event_subs_rate
      @event = Event.find(params[:id])
      @event_athletes = @event.athletes.subscriptions.where('approved = ?', true).count
      @max_athletes = @event.max_subscriptions
      @rate = (@event_athletes / @max_athletes) * 100
    end

    def event_recall_rate(event)
      @subscriptions = Membership.where('event_id = ? and approved = ?', event.id, true).count
      @rate = (@subscriptions / event.max_subscriptions) * 100
    end

    def set_event
      @event = current_organization.events.find_by!(id: params[:id])
    end

    def set_global_event
      @g_event = Event.find(params[:id])
    end
  end
end
