module V1
  # app/controllers/v1/vouchers_controller.rb
  class VouchersController < ApplicationController
    skip_before_action :authorize_athlete_request
    skip_before_action :authorize_admin_request

    def create
      @event = Event.find(params[:id])
      vouche_number = 15.times.map { rand(0..9) }.join.to_s
      discount = params[:discount]
      @voucher = Voucher.create!(number: vouche_number, event_id: @event.id, discount: discount)
      json_response(@voucher)
    end

  end
end
