# app/controllers/v1/teams_controller.rb
module V1
  class TeamsController < ApplicationController
    skip_before_action :authorize_organization_request
    skip_before_action :authorize_admin_request
    before_action :set_team, only: %i[show update destroy]
    before_action :set_team_id, only: %i[join_team approve_join]

    # GET /athletes/:athlete_id/dependents
    def index
      @teams = current_athlete.teams
      json_response(@teams)
    end

    # # GET /athletes/:athlete_id/dependents/:id
    def show
      json_response(@team)
    end

    # # POST /athletes/:athlete_id/dependents
    def create
      @athlete = current_athlete.teams.create!(team_params)
      json_response(@athlete, :created)
    end

    # PUT /athletes/:athlete_id/dependents/:id
    def update
      @team.update(team_params)
      head :no_content
    end

    # DELETE /athletes/:athlete_id/dependents/:id
    def destroy
      @team.destroy
      head :no_content
    end

    def join_team
      if @team.visibility == 'open'
        if current_athlete.my_team.blank?
          current_athlete.update_attribute(:my_team, @team.id)
          json_response(message: "You are pending approval to join team")
        else
          json_response(message: 'You already joined a team')
        end
      else
        json_response(message: 'Sorry, this is a private team')
      end
    end

    def pending_approval
      @athletes = Athlete.all.where('my_team in (?) and approved = ?', current_athlete.teams.map{ |team| team.id }, false)
      json_response(@athletes)
    end

    def team_athletes
      @team = Team.find(params[:id])
      @athletes = Athlete.all.where('my_team = ? and approved = ?', @team.id, true)
      json_response(@athletes)
    end

    def approve_join
      if @team.owner_id == current_athlete.id
        @athlete = Athlete.all.where('id = (?) and approved = ?', params[:athlete_id], false)
        @athlete.update_attributes(my_team: @team.id, approved: true)
        json_response(message: "The athlete #{Athlete.find(params[:athlete_id]).name} joinned to #{@team.team_name} team")
      else
        json_response(message: 'Only the owner team can approve users')
      end
    end

    def leave_team
      if current_athlete.my_team.blank?
        json_response(message: 'You are not joinned at a team')
      else
        @team = Team.find(params[:id])
        current_athlete.update_attributes(my_team: nil, approved: false)
        json_response("You leave the #{@team.team_name} team")
      end
    end

    private
    def team_params
      params.permit(
        :team_name,
        :team_info,
        :team_image,
        :mote,
        :mode,
        :state,
        :visibility
      )
    end

    def set_team
      @team = current_athlete.teams.find_by!(id: params[:id])
    end

    def set_team_id
      @team = Team.find(params[:id])
    end
  end
end
