class ModalitiesController < ApplicationController

  skip_before_action :authorize_admin_request
  skip_before_action :authorize_athlete_request
  skip_before_action :authorize_organization_request

  def index
    @modalities = Modality.all
    json_response(@modalities)
  end
end
