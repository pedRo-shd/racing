# app/controllers/athletes_controller.rb
class AthletesController < ApplicationController
  skip_before_action :authorize_athlete_request, only: %i[create avatar_url reset_athlete_password get_athlete]
  skip_before_action :authorize_organization_request
  skip_before_action :authorize_admin_request
  # POST /signup
  # return authenticated token upon signup
  def athlete
    @athlete = current_athlete
    json_response(@athlete)
  end

  def athlete_events
    @events = current_athlete.events
    json_response(@events)
  end

  def get_athlete
    @athlete = Athlete.find(params[:id])
    json_response(@athlete)
  end


  def create
    athlete = Athlete.create!(athlete_params)
    athlete_auth_token = AuthenticateAthlete.new(athlete.email, athlete.password).athlete_call
    response = { message: Message.account_created, auth_token: athlete_auth_token, atlhete: athlete }
    if params[:file]
      params[:athletes][:avatar] = params[:file]
    end
    json_response(response, :created)
  end

  def update
    current_athlete.update(athlete_params)
    response = { message: Message.account_updated }
    json_response(response, :created)
  end

  def avatar_url
    @athlete = Athlete.find(params[:id])
    url = @athlete.avatar.url(:default, timestamp: false)
    json_response(avatar_url: url)
  end

  def reset_athlete_password
    if @athlete = Athlete.find_by_email(params[:athlete_email])
      password = 8.times.map { rand(1..9) }.join.to_s
      if @athlete.update_attributes(password: password, password_confirmation: password)
        @password = password
        AthleteResetMailer.pass_reset_athlete(@athlete, @password).deliver
        json_response(message: "Reset password email was sent to #{@athlete.email}")
      else
        json_response(message: "Can't change the password, Contact the app administrator...")
      end
    else
      json_response(message: "Email not found...")
    end
  end

  def to_be_recalled
    if current_athlete
      @events = current_athlete.events.where('be_recalled = ?', true)
      json_response(@events)
    else
      json_response(message: 'Action allowed only for athletes')
    end
  end

  def accept_recall
    if current_athlete
      @event = Event.find(params[:event_id])
      @subscription = Membership.where('athlete_id = ? and event_id = ?', current_athlete.id, @event.id)
      @subscription.update_all(recall_authorize: true)
      json_response(message: "You accept recall the event #{@event.name} to #{@event.new_event_date}")
    else
      json_response(message: 'Action allowed only for athletes')
    end
  end

  def subscription_to_pay
    @invoice = Invoice.where('athlete_id = ? and processed = ?', current_athlete.id, false)
    json_response(@invoice)
  end

  private

  def athlete_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation,
      :gender,
      :cpf,
      :birth_date,
      :avatar,
      :my_team,
      :approved
    )
  end
end
