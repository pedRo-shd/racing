class CitiesController < ApplicationController

  skip_before_action :authorize_admin_request
  skip_before_action :authorize_athlete_request
  skip_before_action :authorize_organization_request

  def states_br
    @states = []

    CS.states(:br).each do |key, value|
      @states << "#{key}"
    end
    json_response(@states)
  end

  def cities_br
    @cities = []

    @cities << CS.states(:br).keys.flat_map { |state| CS.cities(state, :br) }
    json_response(@cities)
  end
end
