class Api::V1::OrdersController < ApplicationController

  def create
    @order = Order.new(order_params)

    pagarme_transaction = PagarMe::Transaction.new(params[:card_hash])
    pagarme_transaction.amount = @order.amount

    begin
      pagarme_transaction.charge
    rescue PagarMe::PagarMeError => e
      puts e.inspect
      redirect_to :back, notice: "Erro: #{e.message}"
      return
    end
  end

  private

  def order_params
    params.require(:order).permit(:id, :user_id)
  end
end
