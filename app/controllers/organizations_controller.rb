# app/controllers/organizations_controller.rb
require 'httparty'
class OrganizationsController < ApplicationController
  skip_before_action :authorize_organization_request, only: %i[create reset_organization_password]
  skip_before_action :authorize_athlete_request
  skip_before_action :authorize_admin_request

  before_action :set_organization, only: %i[show]

  include HTTParty

  PagarMe.api_key        = 'ak_test_ljnUC4CBqNzjPVaHx9RVIKAd2n6Jcx'
  PagarMe.encryption_key = 'ek_test_SzdoodIKVJ3l3uMOjB6Pc7UxxMyw3s'

  # POST /signup
  # return authenticated token upon signup

  def organization
    @organization = current_organization
    json_response(@organization)
  end

  def create
    organization = Organization.create!(organization_params)
    organization_auth_token = AuthenticateOrganization.new(organization.email, organization.password).organization_call
    response = { message: Message.account_created, auth_token: organization_auth_token }
    json_response(response, :created)
    bank_account_create(organization)
  end

  def show

  end

  def update
    current_organization.update(organization_params)
    response = { message: Message.account_updated }
    json_response(response, :created)
  end

  def reset_organization_password
    if @organization = Organization.find_by_email(params[:organization_email])
      password = 8.times.map { rand(1..9) }.join.to_s
      if @organization.update_attributes(password: password, password_confirmation: password)
        @password = password
        OrganizationResetMailer.pass_reset_organization(@organization, @password).deliver
        json_response(message: "Reset password email was sent to #{@organization.email}")
      else
        json_response(message: "Can't change the password, Contact the app administrator...")
      end
    else
      json_response(message: "Email not found...")
    end
  end

  def get_all_subscriptions
    if  current_organization.present?
      @subscriptions = current_organization.memberships.pluck(:athlete_id)
      @athletes = Athlete.where('id IN (?)', @subscriptions)
      json_response(@athletes)
    else
      json_response(message: 'You need to be an organization to see this')
    end
  end

  def org_subscriptions_status
    @membership = current_organization.memberships.where('event_id = ?', params[:event_id])
    json_response(@membership)
  end

  def org_co_subscriptions_status
    @comembership = current_organzation.comemberships.where('event_id = ?', params[:event_id])
    json_response(@comembership)
  end

  private

  def set_organization
    @organization = Organization.find(params[:id])
  end


  def bank_account_create(organization)
    organization = organization
    @api_key = 'ak_test_ljnUC4CBqNzjPVaHx9RVIKAd2n6Jcx'
    @create_account_url = "https://api.pagar.me/1/bank_accounts?api_key=#{@api_key}"
    @bank_account = HTTParty.post(@create_account_url,
      body: { bank_code:       organization.bank_code,
              agencia:         organization.bank_agency,
              agencia_dv:      organization.bank_agency_dv,
              conta:           organization.bank_account,
              conta_dv:        organization.bank_account_dv,
              legal_name:      organization.bank_account_name,
              document_number: organization.bank_document
  }.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
    organization.update_attributes(account_id: @bank_account['id'])
    create_recipent(organization)
  end


  def create_recipent(organization)
    organization = organization
    @api_key = 'ak_test_ljnUC4CBqNzjPVaHx9RVIKAd2n6Jcx'
    @create_recipent_url = "https://api.pagar.me/1/recipients?api_key=#{@api_key}"
    @recipent = HTTParty.post(@create_recipent_url,
      body: { transfer_enable: false,
              bank_account_id: organization.account_id
  }.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
    organization.update_attributes(recipent_id: @recipent['id'])
  end

  def organization_params
    params.permit(
      :name,
      :email,
      :password,
      :password_confirmation,
      :street,
      :number,
      :neighbor,
      :city,
      :state,
      :cpf,
      :cnpj,
      :phone,
      :second_phone,
      :bank_code,
      :bank_agency,
      :bank_agency_dv,
      :bank_account,
      :bank_account_dv,
      :bank_account_name,
      :bank_document,
      :photo
    )
  end
end
