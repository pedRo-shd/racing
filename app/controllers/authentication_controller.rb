
class AuthenticationController < ApplicationController
  skip_before_action :authorize_athlete_request, only: [:athlete_authenticate, :organization_authenticate, :admin_authenticate]
  skip_before_action :authorize_organization_request, only: [:athlete_authenticate, :organization_authenticate, :admin_authenticate]
  skip_before_action :authorize_admin_request, only: [:athlete_authenticate, :organization_authenticate, :admin_authenticate]
  # return auth token once athlete is authenticated
  def athlete_authenticate
    athlete_auth_token =
      AuthenticateAthlete.new(auth_params[:email], auth_params[:password]).athlete_call
    json_response(auth_token: athlete_auth_token)
  end

  # return auth token once organization is authenticated
  def organization_authenticate
    organization_auth_token =
      AuthenticateOrganization.new(auth_params[:email], auth_params[:password]).organization_call
    json_response(auth_token: organization_auth_token)
  end

  # return auth token once organization is authenticated
  def admin_authenticate
    admin_auth_token =
      AuthenticateAdmin.new(auth_params[:email], auth_params[:password]).admin_call
    json_response(auth_token: admin_auth_token)
  end

  private

  def auth_params
    params.permit(:email, :password)
  end
end
