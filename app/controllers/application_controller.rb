# app/controllers/application_controller.rb
class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  # called before every action on controllers
  before_action :authorize_athlete_request
  before_action :authorize_organization_request
  before_action :authorize_admin_request
  attr_reader :current_athlete, :current_organization, :current_admin

  private

  # Check for valid request token and return athlete
  def authorize_athlete_request
    @current_athlete = (AuthorizeApiRequest.new(request.headers).athlete_call)[:athlete]
  end

  # Check for valid request token and return organization
  def authorize_organization_request
    @current_organization = (AuthorizeApiRequest.new(request.headers).organization_call)[:organization]
  end

  # Check for valid request token and return admin
  def authorize_admin_request
    @current_admin = (AuthorizeApiRequest.new(request.headers).admin_call)[:admin]
  end
end
