# app/controllers/admins_controller.rb
require 'httparty'
class AdminsController < ApplicationController
  skip_before_action :authorize_admin_request, only: %i[create reset_admin_password get_events search]
  skip_before_action :authorize_athlete_request, only: %i[create update to_be_canceled get_events get_athletes subscriptions_status co_subscriptions_status search]
  skip_before_action :authorize_organization_request, only: %i[create update to_be_canceled get_events get_athletes subscriptions_status co_subscriptions_status search]

  include HTTParty
  # POST /signup
  # return authenticated token upon signup

  def create
    admin = Admin.create!(admin_params)
    admin_auth_token = AuthenticateAdmin.new(admin.email, admin.password).admin_call
    response = { message: Message.account_created, auth_token: admin_auth_token }
    json_response(response, :created)
    admin_bank_account_create(admin)
  end

  def update
    current_admin.update(admin_params)
    response = { message: Message.account_updated }
    json_response(response, :created)
  end

  def get_athletes
    @athletes = Athlete.all if current_admin
    json_response(@athletes)
  end

  def get_events
    @events = Event.all
    json_response(@events)
  end

  def get_organizations
    @organizations = Organization.all if current_admin
    json_response(@organizations)
  end

  def to_be_canceled
    if current_admin
      @events = Event.all.where('be_canceled = ? and status <> ?', true, 3)
      json_response(@events)
    else
      json_response(message: 'Action allowed only for admins')
    end
  end

  def admin_to_be_recalled
    if current_admin
      @events = Event.all.where('be_canceled = ?', true)
      json_response(@events)
    else
      json_response(message: 'Action allowed only for athletes')
    end
  end


  def admin_cancel_event
    @event = Event.find(params[:event_id])
    if @event.update_attributes(status: 'canceled', be_canceled: false)
      json_response(message: 'Event canceled!')
    else
      json_response(message: 'Event not canceled. Contact the administrator.')
    end
  end

  def reset_admin_password
    if @admin = Admin.find_by_email(params[:admin_email])
      password = 8.times.map { rand(1..9) }.join.to_s
      if @admin.update_attributes(password: password, password_confirmation: password)
        @password = password
        AdminResetMailer.pass_reset_admin(@admin, @password).deliver
        json_response(message: "Reset password email was sent to #{@admin.email}")
      else
        json_response(message: "Can't change the password, Contact the app administrator...")
      end
    else
      json_response(message: "Email not found...")
    end
  end

  def subscriptions_status
    @membership = Membership.where('event_id = ?', params[:event_id])
    json_response(@membership)
  end

  def co_subscriptions_status
    @comembership = Comembership.where('event_id = ?', params[:event_id])
    json_response(@comembership)
  end

  def search
    search_condition = params[:search] || '*'
    page = params[:page] || 1

    days_to_start = Date.current..Date.current + 7.day if params[:seven_days] == 'true'
    days_to_start = Date.current..Date.current + 30.day if params[:thirty_days] == 'true'
    days_to_start = Date.current..Date.current + 60.day if params[:sixty_days] == 'true'
    days_to_start = Date.current..Date.current + 90.day if params[:ninety_days] == 'true'
    days_to_start = Date.current..Date.current + 120.day if params[:one_hundred_twenty_days] == 'true'
    days_to_start = Date.strptime(params[:begindate], "%m/%d/%Y")..Date.strptime(params[:enddate], "%m/%d/%Y") if params[:begindate].present? && params[:enddate].present?

    conditions = {
      status: :open,
      start_city: params[:start_city],
      event_date: days_to_start,
      start_state: params[:start_state]
    }

    @events = (Event.search search_condition, where: conditions.select {|k,v| v.present?},  page: page, per_page: 18)
    json_response(@events)
  end

private

  def admin_bank_account_create(admin)
    admin = admin
    @api_key = 'ak_test_ljnUC4CBqNzjPVaHx9RVIKAd2n6Jcx'
    @create_account_url = "https://api.pagar.me/1/bank_accounts?api_key=#{@api_key}"
    @bank_account = HTTParty.post(@create_account_url,
      body: { bank_code:       admin.bank_code,
              agencia:         admin.bank_agency,
              agencia_dv:      admin.bank_agency_dv,
              conta:           admin.bank_account,
              conta_dv:        admin.bank_account_dv,
              legal_name:      admin.bank_account_name,
              document_number: admin.bank_document
  }.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )

    admin.update_attributes(account_id: @bank_account['id'])
    create_recipent (admin)
  end


  def create_recipent(admin)
    admin = admin
    @api_key = 'ak_test_ljnUC4CBqNzjPVaHx9RVIKAd2n6Jcx'
    @create_recipent_url = "https://api.pagar.me/1/recipients?api_key=#{@api_key}"
    @recipent = HTTParty.post(@create_recipent_url,
      body: { transfer_enable: false,
              bank_account_id: admin.account_id
  }.to_json,
      headers: { 'Content-Type' => 'application/json' }
    )
    admin.update_attributes(recipent_id: @recipent['id'])
  end



  def admin_params
    params.require(:admin).permit(
      :name,
      :email,
      :password,
      :password_confirmation,
      :bank_code,
      :bank_agency,
      :bank_agency_dv,
      :bank_account,
      :bank_account_dv,
      :bank_account_name,
      :bank_document,
      :admin_with_token
    )
  end
end
