class ContactsController < ApplicationController

  def create
    organizer = Organization.joins(:events).where(events: params[:event_id])

    @event_contacts = EventContactsMailer.more_info_event(
      contact_params, organizer, event
    ).deliver

    json_response @event_contacts
  end


  private

  def contact_params
    params.permit(:name, :email, :title, :description)
  end
end
