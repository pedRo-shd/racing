class Membership < ApplicationRecord
  enum status: %i[paid waiting_payment refused]
  after_initialize :init

  belongs_to :athlete
  belongs_to :event

  def init
    if self.new_record?
      self.approved = false
    end
  end
end
