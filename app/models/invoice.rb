class Invoice < ApplicationRecord
  belongs_to :athlete
  belongs_to :event
  has_many :checkouts
end
