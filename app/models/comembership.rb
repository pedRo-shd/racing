class Comembership < ApplicationRecord
  enum status: %i[paid waiting_payment refused]
  belongs_to :dependent
  belongs_to :event
end
