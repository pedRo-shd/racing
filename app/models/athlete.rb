# app/models/athletes.rb
class Athlete < ApplicationRecord
  after_initialize :init
  has_secure_password
  has_many :dependents, dependent: :destroy
  has_many :teams, class_name: 'Team', primary_key: 'id', foreign_key: 'owner_id'
  has_many :memberships
  has_many :events, through: :memberships, dependent: :destroy
  has_many :invoices

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  # validates_attachment :avatar, :content_type => { :content_type => "image/jpg/jpeg/png" }
  do_not_validate_attachment_file_type :avatar

  validates_presence_of :name,
                        :email,
                        :password_digest

  validates_uniqueness_of :email, on: :create, message: "Email must be uniq"

  def init
    self.approved = false if approved.nil?
  end
end
