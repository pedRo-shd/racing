class HolderImage < ApplicationRecord
  belongs_to :event
  validates_presence_of :image

  mount_base64_uploader :photo, PhotoUploader
end
