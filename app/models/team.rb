class Team < ApplicationRecord
  after_initialize :init

  belongs_to :athlete, class_name: 'Athlete', foreign_key: 'owner_id'
  has_many :dependents
  validates_presence_of :team_name, :mote, :mode, :team_info
  enum visibility: %i[open closed]


  def init
    self.visibility = 'open'
  end
end
