require 'httparty'

class AccessIsNotValid    < StandardError; end

class Admin < ApplicationRecord
  has_secure_password

  attr_accessor :admin_with_token
  validates_presence_of :email, :name
  include HTTParty

  before_create :should_be_valid

  def should_be_valid
    raise AccessIsNotValid, "Access Denied" if admin_with_token != "fd9741e2-60a5-46d9-bf17-0ba7173246af"
  end
end
