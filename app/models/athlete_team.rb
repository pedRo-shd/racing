class AthleteTeam < ApplicationRecord
  belongs_to :athlete
  belongs_to :my_teams, class_name: 'Team', foreign_key: 'team_id'
end
