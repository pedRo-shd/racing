class Event < ApplicationRecord
  # Opções para o status: aguardando aberto fechado cancelado e remarcado
  enum status: %i[on_hold open closed canceled recalled]
  before_create :geocode_start
  after_initialize :init

  belongs_to :organization
  belongs_to :modality
  has_many :memberships
  has_many :comemberships
  has_many :athletes, through: :memberships, dependent: :destroy
  has_many :dependents, through: :comemberships, dependent: :destroy
  has_many :holder_images, dependent: :destroy
  has_many :vouchers
  has_many :invoices

  mount_base64_uploader :photo, PhotoUploader

  accepts_nested_attributes_for :modality


  validates_presence_of :name, :start_street, :event_date, :event_info, :subscription_end, :start_hours,
                        :max_subscriptions, :modality, :organization, :price

  scope :three_first, -> { where('event_date is not null').order(event_date: :desc).limit(3) }

  def close_event
    @events = Event.where('status = ? and subscription_end = ?', 1, Date.current)
    @events.each do |event|
      event.update_attributes(status: 'closed')
    end
  end

  searchkick

  def search_data
    {
      name: name,
      status: status,
      start_city: start_city,
      start_state: start_state,
      start_neighbor: start_neighbor,
      event_date: event_date,
      modality_name: modality.name,
      subscription_end: subscription_end
    }
  end

  private

  def geocode_start
    if start_address
      geocoded = Geocoder.search(start_address).first
      if geocoded
        self.start_lat = geocoded.latitude
        self.start_long = geocoded.longitude
      end
    end
    geocode_finish()
  end

  def geocode_finish
    if finish_address
      geocoded = Geocoder.search(finish_address).first
      if geocoded
        self.finish_lat = geocoded.latitude
        self.finish_long = geocoded.longitude
      end
    end
  end


  def init
    if self.new_record?
      self.be_canceled ||= false
    end
  end

  def start_address
    "#{start_street},
    #{start_number},
    #{start_neighbor},
    #{start_city},
    #{start_state}"
  end

  def finish_address
    "#{finish_street},
    #{finish_number},
    #{finish_neighbor},
    #{finish_city},
    #{finish_state}"
  end

end
