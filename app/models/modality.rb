class Modality < ApplicationRecord

  has_many :events

  mount_base64_uploader :photo, PhotoUploader

end
