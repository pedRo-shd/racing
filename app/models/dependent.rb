# app/models/dependent.rb
class Dependent < ApplicationRecord
  belongs_to :athlete
  belongs_to :team, optional: true
  has_many :comemberships
  has_many :events, through: :comemberships, dependent: :destroy

  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png" 
  # validates_attachment :avatar, :content_type => { :content_type => "image/jpg/jpeg/png" }
  do_not_validate_attachment_file_type :avatar
  
  validates_presence_of :name, :cpf

end
