require 'httparty'
class Organization < ApplicationRecord
  include HTTParty
  has_secure_password
  has_many :events
  has_many :memberships, through: :events
  validates_presence_of :email, :name

  mount_base64_uploader :photo, PhotoUploader
end
