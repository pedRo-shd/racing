class Voucher < ApplicationRecord
  belongs_to :event

  validates_presence_of :number
end
