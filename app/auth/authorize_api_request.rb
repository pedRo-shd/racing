class AuthorizeApiRequest
  def initialize(headers = {})
    @headers = headers
  end

  # Service entry point - return valid athlete object
  def athlete_call
    {
      athlete: athlete
    }
  end

  # Service entry point - return valid organization object
  def organization_call
    {
      organization: organization
    }
  end

  # Service entry point - return valid admin object
  def admin_call
    {
      admin: admin
    }
  end


  private

  attr_reader :headers

  def athlete
    # check if user is in the database
    # memoize user object
    @athlete ||= Athlete.find(decoded_auth_token[:athlete_id]) if decoded_auth_token
    # handle user not found
  rescue ActiveRecord::RecordNotFound => e
    # raise custom error
    raise(
      ExceptionHandler::InvalidToken,
      ("#{Message.invalid_token} #{e.message}")
    )
  end


  def organization
    # check if organization is in the database
    # memoize organization object
    @organization ||= Organization.find(decoded_auth_token[:organization_id]) if decoded_auth_token
    # handle user not found
  rescue ActiveRecord::RecordNotFound => e
    # raise custom error
    raise(
      ExceptionHandler::InvalidToken,
      ("#{Message.invalid_token} #{e.message}")
    )
  end

  def admin
    # check if admin is in the database
    # memoize admin object
    @admin ||= Admin.find(decoded_auth_token[:admin_id]) if decoded_auth_token
    # handle user not found
  rescue ActiveRecord::RecordNotFound => e
    # raise custom error
    raise(
      ExceptionHandler::InvalidToken,
      ("#{Message.invalid_token} #{e.message}")
    )
  end


  # decode authentication token
  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken.decode(http_auth_header)
  end

  # check for token in `Authorization` header
  def http_auth_header
    if headers['Authorization'].present?
      return headers['Authorization'].split(' ').last
    end
      raise(ExceptionHandler::MissingToken, Message.missing_token)
  end
end
