class AuthenticateOrganization
  def initialize(email, password)
    @email = email
    @password = password
  end

  # Service entry point
  def organization_call
    JsonWebToken.encode(organization_id: organization.id) if organization
  end

  private

  attr_reader :email, :password

  # verify user credentials
  def organization
    organization = Organization.find_by(email: email)
    return organization if organization && organization.authenticate(password)
    # raise Authentication error if credentials are invalid
    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end
end
