class AuthenticateAdmin
  def initialize(email, password)
    @email = email
    @password = password
  end

  # Service entry point
  def admin_call
    JsonWebToken.encode(admin_id: admin.id) if admin
  end

  private

  attr_reader :email, :password

  # verify user credentials
  def admin
    admin = Admin.find_by(email: email)
    return admin if admin && admin.authenticate(password)
    # raise Authentication error if credentials are invalid
    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end
end
