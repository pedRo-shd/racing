class AuthenticateAthlete
  def initialize(email, password)
    @email = email
    @password = password
  end

  # Service entry point
  def athlete_call
    JsonWebToken.encode(athlete_id: athlete.id) if athlete
  end

  private

  attr_reader :email, :password

  # verify user credentials
  def athlete
    athlete = Athlete.find_by(email: email)
    return athlete if athlete && athlete.authenticate(password)
    # raise Authentication error if credentials are invalid
    raise(ExceptionHandler::AuthenticationError, Message.invalid_credentials)
  end
end
