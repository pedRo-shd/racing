source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# Bundle e2dge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# Token based authentication for Rails JSON APIs. Designed to work with jToker and ng-token-auth.
gem 'devise_token_auth'
# OmniAuth is a flexible authentication system utilizing Rack middleware. https://omniauth.io
gem 'omniauth'
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
# Gem to use Jason Web Token
gem 'jwt'
# Gem to serialization os JSON responses
gem 'active_model_serializers', '~> 0.10.0'
# Gem to handle with files upload
gem 'paperclip'
# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'
# Gem to handle environment variables
gem 'figaro'
# Gemt o handle the deolocation feature
gem 'geocoder'
# Gem to handle the payment gateway
gem 'pagarme'
# Gem to handle HTTP
gem 'httparty'
# Gen to handle background jobs
gem 'whenever', require: false

gem 'searchkick'

gem 'city-state'

gem 'carrierwave'
gem 'carrierwave-base64'
gem 'mini_magick'

gem 'will_paginate'

gem 'cloudinary'

group :development, :test do
  gem 'rspec-rails', '~> 3.5'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'shoulda-matchers', '~> 3.1'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
