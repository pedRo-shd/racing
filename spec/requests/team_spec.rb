require 'rails_helper'

RSpec.describe 'Teams', type: :request do
  let(:athlete) { create(:athlete) }
  let!(:team) { create(:team, owner_id: athlete.id) }
  let(:team_id) { team.id }
  let(:headers) { athlete_valid_headers }

  describe 'GET /teams' do
    # update request with headers
    before { get '/teams', params: {}, headers: headers }

    it 'returns teams' do
      expect(json).not_to be_empty
      expect(json.size).to eq(1)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /teams/:id' do
    before { get "/teams/#{team_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the todo' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(team_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:team_id) { 10 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Team/)
      end
    end
  end

  describe 'POST /teams' do
     let(:valid_attributes) do
      # send json payload
      { team_name: 'Some Name', athlete_id: athlete.id.to_s, mode: 'mountainbike', mote: 'wherever', team_info: 'some more wherever' }.to_json
    end

    context 'when request is valid' do
      before { post '/teams', params: valid_attributes, headers: headers }

      it 'creates a team' do
        expect(json['team_name']).to eq('Some Name')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when request is invalid' do
      let(:valid_attributes) { { mote: nil }.to_json }
      before { post '/teams', params: valid_attributes, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Team name can't be blank/)
      end
    end
  end

  describe 'PUT /teams/:id' do
    let(:valid_attributes) { { team_name: 'Silvester Stalone' }.to_json }

    context 'when the record exists' do
      before { put "/teams/#{team_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /teams/:id' do
    before { delete "/teams/#{team_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

  # describe 'GET /teams/:id/join_team' do
  #   before { get "/teams/#{team_id}/join_team" }
  #   it 'returns status code 200' do
  #     expect(response).to have_http_status(200)
  #   end
  # end

  # describe 'GET /teams/:id/leave_team' do
  #   before { get "/teams/#{team_id}/leave_team" }
  #   it 'returns status code 200' do
  #     expect(response).to have_http_status(200)
  #   end
  # end

end
