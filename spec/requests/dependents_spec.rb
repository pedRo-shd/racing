require 'rails_helper'

RSpec.describe 'Dependents', type: :request do
  let(:athlete) { create(:athlete) }
  let(:team) { create(:team, owner_id: athlete.id) }
  let!(:dependents) { create_list(:dependent, 10, athlete_id: athlete.id, team_id: team.id) }
  let(:dependent_id) { dependents.first.id }
  let(:headers) { athlete_valid_headers }

  describe 'GET /dependents' do
    # update request with headers
    before { get '/dependents', params: {}, headers: headers }

    it 'returns dependents' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /dependents/:id' do
    before { get "/dependents/#{dependent_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the dependent' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(dependent_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:dependent_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Dependent/)
      end
    end
  end

  describe 'POST /dependents' do
    let(:valid_attributes) do
      # send json payload
      { name: 'Some Name', athlete_id: athlete.id.to_s, cpf: '123456798', team_id: team.id.to_s }.to_json
    end

    context 'when request is valid' do
      before { post '/dependents', params: valid_attributes, headers: headers }

      it 'creates a dependent' do
        expect(json['name']).to eq('Some Name')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when request is invalid' do
      let(:valid_attributes) { { name: nil }.to_json }
      before { post '/dependents', params: valid_attributes, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Name can't be blank/)
      end
    end
  end

  describe 'PUT /dependents/:id' do
    let(:valid_attributes) { { name: 'Silvester Stalone' }.to_json }

    context 'when the record exists' do
      before { put "/dependents/#{dependent_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /dependents/:id' do
    before { delete "/dependents/#{dependent_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
 end

end
