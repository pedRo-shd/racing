require 'rails_helper'

RSpec.describe AuthorizeApiRequest do
  # Create test athlete
  let(:athlete) { create(:athlete) }
  # Mock `Authorization` header
  let(:header) { { 'Authorization' => athlete_token_generator(athlete.id) } }
  # Invalid request subject
  subject(:invalid_request_obj) { described_class.new({}) }
  # Valid request subject
  subject(:request_obj) { described_class.new(header) }

  # Test Suite for AuthorizeApiRequest#call
  # This is our entry point into the service class
  describe '#athlete_call' do
    # returns athlete object when request is valid
    context 'when valid request' do
      it 'returns athlete object' do
        result = request_obj.athlete_call
        expect(result[:athlete]).to eq(athlete)
      end
    end

    # returns error message when invalid request
    context 'when invalid request' do
      context 'when missing token' do
        it 'raises a MissingToken error' do
          expect { invalid_request_obj.athlete_call }
            .to raise_error(ExceptionHandler::MissingToken, 'Missing token')
        end
      end

      context 'when invalid token' do
        subject(:invalid_request_obj) do
          # custom helper method `token_generator`
          described_class.new('Authorization' => athlete_token_generator(5))
        end

        it 'raises an InvalidToken error' do
          expect { invalid_request_obj.athlete_call }
            .to raise_error(ExceptionHandler::InvalidToken, /Invalid token/)
        end
      end

      context 'when token is expired' do
        let(:header) { { 'Authorization' => athlete_expired_token_generator(athlete.id) } }
        subject(:request_obj) { described_class.new(header) }

        it 'raises ExceptionHandler::ExpiredSignature error' do
          expect { request_obj.athlete_call }.to raise_error(ExceptionHandler::ExpiredSignature, /Signature has expired/)
        end
      end
    end
  end
end