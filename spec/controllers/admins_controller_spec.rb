require 'rails_helper'

RSpec.describe Api::V1::AdminsController, type: :controller do
  describe "GET #check_availability" do
    before do
      request.env["HTTP_ACCEPT"] = 'application/json'
    end

    context "with a property associated a search query" do
      it "receive one result when property active" do
        @organization = create(:organization)
        @event = create(:event, start_city: 'The outro', status: :open, organization_id: @organization.id)
        # Force reindex
        Event.reindex

        get :search, params: {search: 'The outro'}
        expect(JSON.parse(response.body).count).to eql(1)
      end
    end

  end
end
