# spec/factories/admins.rb
FactoryBot.define do
  factory :admin do
    name { Faker::Name.name }
    email 'foo@bar.com'
    password 'foobar123'
    bank_code "001"
    bank_agency "1235"
    bank_agency_dv '5'
    bank_account "654321"
    bank_account_dv "5"
    bank_document "99879469844"
    bank_account_name "Test Bank Account"
  end
end
