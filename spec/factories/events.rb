# spec/factories/events.rb
FactoryBot.define do
  factory :event do
    name { Faker::StarWars.character }
    event_info { Faker::LordOfTheRings.character }
    subscription_start { Faker::Date.backward(14) }
    subscription_end { Faker::Date.forward(23) }
    max_subscriptions 50
  end
end
