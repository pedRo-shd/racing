# spec/factories/teams.rb
FactoryBot.define do
  factory :team do
    team_name { Faker::StarWars.character }
    mote { Faker::DragonBall.character }
    mode { Faker::DragonBall.character }
    team_info { Faker::LordOfTheRings.character }
  end
end