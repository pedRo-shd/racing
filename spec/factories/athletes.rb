# spec/factories/athletes.rb
FactoryBot.define do
  factory :athlete do
    name { Faker::Name.name }
    email 'foo@bar.com'
    password 'foobar123'
    street 'foobar street'
  end
end
