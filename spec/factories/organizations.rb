# spec/factories/organizations.rb
FactoryBot.define do
  factory :organization do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password '12345678'
    password_confirmation '12345678'
    street { Faker::Address.street_name }
    number { Faker::Number.number(4) }
    neighbor { Faker::Address.community }
    city { Faker::Address.city }
    state 'SP'
    cpf { Faker::Number.number(11) }
    phone { Faker::Number.number(10) }
    bank_code "001"
    bank_agency "1235"
    bank_agency_dv '5'
    bank_account "654321"
    bank_account_dv "5"
    bank_account_type 'conta_corrent'
    bank_document "99879469844"
    bank_account_name "Test Bank Account"
  end
end
