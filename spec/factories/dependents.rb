# spec/factories/dependents.rb
FactoryBot.define do
  factory :dependent do
    name { Faker::StarWars.character }
    cpf { Faker::Number.number(11) }
  end
end