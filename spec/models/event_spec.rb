require 'rails_helper'

RSpec.describe Event, type: :model do
  # Association tests
  it { should belong_to(:organization) }
  it { should have_many(:holder_images) }
  it { should have_many(:athletes).through(:memberships) }
  it { should have_many(:dependents).through(:comemberships) }
  it { should have_many(:invoices) }
  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:event_info) }
end
