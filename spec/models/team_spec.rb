require 'rails_helper'

RSpec.describe Team, type: :model do
  # Association tests
  it { should belong_to(:athlete) }
  # Validation tests
  it { should validate_presence_of(:team_name) }
  it { should validate_presence_of(:mote) }
  it { should validate_presence_of(:mode) }
  it { should validate_presence_of(:team_info) }
end
