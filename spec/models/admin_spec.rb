require 'rails_helper'

RSpec.describe Admin, type: :model do
  # Valiadtions tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
end
