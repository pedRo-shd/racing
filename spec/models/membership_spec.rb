require 'rails_helper'

RSpec.describe Membership, type: :model do
  # Association tests
  it { should belong_to(:athlete) }
  it { should belong_to(:event) }
end
