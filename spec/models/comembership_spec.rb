require 'rails_helper'

RSpec.describe Comembership, type: :model do
  # Relational tests
  it { should belong_to(:event) }
  it { should belong_to(:dependent) }  
end
