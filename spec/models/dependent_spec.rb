require 'rails_helper'

RSpec.describe Dependent, type: :model do
  # Association tests
  it { should belong_to(:athlete) }
  it { should have_many(:events).through(:comemberships) }
  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:cpf) } 
end
