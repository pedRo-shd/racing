require 'rails_helper'

RSpec.describe Voucher, type: :model do
  it { should belong_to(:event) } 

  it { should validate_presence_of(:number) }
end
