require 'rails_helper'

RSpec.describe Athlete, type: :model do
  # Validation tests
  it { should have_many(:dependents).dependent(:destroy) }
  it { should have_many(:teams) }
  it { should have_many(:events).through(:memberships) }
  it { should have_many(:invoices) }

  # ensure name, email and password_digest are present before save
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password_digest) }
end
