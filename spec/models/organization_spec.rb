require 'rails_helper'

RSpec.describe Organization, type: :model do
  # Relational tests
  it { should have_many(:events) }
  # Validation Tests
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:name) }
  
end
