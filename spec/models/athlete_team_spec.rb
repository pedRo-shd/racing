require 'rails_helper'

RSpec.describe AthleteTeam, type: :model do
  it { should belong_to(:athlete) }
  it { should belong_to(:my_teams) }
end
