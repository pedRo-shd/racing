require 'rails_helper'

RSpec.describe HolderImage, type: :model do
  it { should belong_to(:event) }

  it { should validate_presence_of(:image) }
end
