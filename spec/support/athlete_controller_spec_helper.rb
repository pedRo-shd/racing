module AthleteControllerSpecHelper
  # generate tokens from athlete id
  def athlete_token_generator(athlete_id)
    JsonWebToken.encode(athlete_id: athlete_id)
  end

  # generate expired tokens from user id
  def athlete_expired_token_generator(athlete_id)
    JsonWebToken.encode({ athlete_id: athlete_id }, (Time.now.to_i - 10))
  end

  # return valid headers
  def athlete_valid_headers
    {
      "Authorization" => athlete_token_generator(athlete.id),
      "Content-Type" => "application/json"
    }
  end

  # return invalid headers
  def athlete_invalid_headers
    {
      "Authorization" => nil,
      "Content-Type" => "application/json"
    }
  end
end
