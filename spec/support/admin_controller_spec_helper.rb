module AdminControllerSpecHelper
  # generate tokens from admin id
  def admin_token_generator(admin_id)
    JsonWebToken.encode(admin_id: admin_id)
  end

  # generate expired tokens from user id
  def admin_expired_token_generator(admin_id)
    JsonWebToken.encode({ admin_id: admin_id }, (Time.now.to_i - 10))
  end

  # return valid headers
  def admin_valid_headers
    {
      "Authorization" => admin_token_generator(admin.id),
      "Content-Type" => "application/json"
    }
  end

  # return invalid headers
  def admin_invalid_headers
    {
      "Authorization" => nil,
      "Content-Type" => "application/json"
    }
  end
end