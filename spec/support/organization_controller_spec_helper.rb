module OrganizationControllerSpecHelper
  # generate tokens from athlete id
  def organization_token_generator(organization_id)
    JsonWebToken.encode(organization_id: organization_id)
  end

  # generate expired tokens from user id
  def athlete_expired_token_generator(organization_id)
    JsonWebToken.encode({ organization_id: organization_id }, (Time.now.to_i - 10))
  end

  # return valid headers
  def organization_valid_headers
    {
      "Authorization" => organization_token_generator(organization.id),
      "Content-Type" => "application/json"
    }
  end

  # return invalid headers
  def organization_invalid_headers
    {
      "Authorization" => nil,
      "Content-Type" => "application/json"
    }
  end
end
