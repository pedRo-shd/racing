# API Racing

- API para se comunicar com App de competição de corridas, contempla integração com API de Pagamentos, Geolocalização com Google Maps e Strava App.

## Requisitos

Dependências necessárias para subir o sistema

- Docker e Docker Compose
- Ruby 2.4.3
- Rails 5.1.4
- Postgresql
- ElasticSearch

### Use o docker para fazer o build das dependências e de todo o projeto, veja logo abaixo

Instale o bundler e todas as gems utilizadas:

      gem install bundler
      bundle install

Instale o Docker no Linux(Ubuntu):

- Atualize seus pacotes rodando:

      sudo apt-get update

- Insira a GPG key do Docker rodando:

      sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

- Adicione o repositório do Docker a APT rodando:

      sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

- Atualize novamente os pacotes:

      sudo apt-get update

- Instale o Docker rodando:

      sudo apt-get install -y docker-engine

- Adicione o Docker à inicialização do sistema:

      sudo systemctl status docker

- Adicione seu usuário ao grupo do Docker rodando:

      sudo usermod -aG docker $(whoami)

- Feche seu terminal e abra novamente.
- Pronto, agora é necessário instalar o Docker Compose, rode:

      curl -L https://github.com/docker/compose/releases/download/1.9.0/docker-compose-`uname -s`-`uname -m` > docker-compose

- Agora para mover o arquivo para sua pasta bin, rode:

      sudo mv docker-compose /usr/local/bin/

- Depois rode:

      chmod +x /usr/local/bin/docker-compose

### Dados iniciais

- Construindo containers do projeto, montando o banco de dados e subindo o projeto no servidor

      docker-compose up --build

- Em outro terminal, rode:

      docker-compose rails db:create db:migrate

- Todas as dependências do projeto estarão disponíveis após esses comandos
