# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180219190601) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email"
    t.string "name"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "bank_code"
    t.string "bank_agency"
    t.string "bank_agency_dv"
    t.string "bank_account"
    t.string "bank_account_dv"
    t.string "bank_account_name"
    t.string "bank_document"
    t.string "account_id"
    t.string "recipent_id"
  end

  create_table "athlete_teams", force: :cascade do |t|
    t.bigint "athlete_id"
    t.bigint "team_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["athlete_id"], name: "index_athlete_teams_on_athlete_id"
    t.index ["team_id"], name: "index_athlete_teams_on_team_id"
  end

  create_table "athletes", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "gender"
    t.string "cpf"
    t.date "birth_date"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "my_team"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean "approved"
    t.string "street"
    t.string "number"
    t.string "neighbor"
    t.string "city"
    t.string "state"
    t.string "zipcode"
    t.string "phone"
  end

  create_table "checkouts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "invoice_id"
    t.index ["invoice_id"], name: "index_checkouts_on_invoice_id"
  end

  create_table "comemberships", force: :cascade do |t|
    t.bigint "dependent_id"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "approved", default: false
    t.integer "status"
    t.index ["dependent_id"], name: "index_comemberships_on_dependent_id"
    t.index ["event_id"], name: "index_comemberships_on_event_id"
  end

  create_table "dependents", force: :cascade do |t|
    t.string "name"
    t.string "gender"
    t.string "cpf"
    t.date "birth_date"
    t.bigint "athlete_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "team_id"
    t.string "avatar_file_name"
    t.string "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["athlete_id"], name: "index_dependents_on_athlete_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.string "start_street"
    t.string "start_number"
    t.string "start_neighbor"
    t.string "start_city"
    t.string "start_state"
    t.date "event_date"
    t.text "event_info"
    t.string "event_image"
    t.string "holders_image"
    t.string "kit_image"
    t.string "event_users_image"
    t.string "contact_email"
    t.string "contact_phone"
    t.string "mode"
    t.text "rules"
    t.bigint "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "kit_image_file_name"
    t.string "kit_image_content_type"
    t.integer "kit_image_file_size"
    t.datetime "kit_image_updated_at"
    t.text "kit_description"
    t.date "subscription_start"
    t.date "subscription_end"
    t.integer "max_subscriptions"
    t.boolean "be_canceled"
    t.integer "status", default: 0, null: false
    t.boolean "be_recalled", default: false
    t.date "recall_start"
    t.date "recall_end"
    t.date "new_event_date"
    t.string "finish_street"
    t.string "finish_number"
    t.string "finish_neighbor"
    t.string "finish_city"
    t.string "finish_state"
    t.float "start_lat"
    t.float "start_long"
    t.float "finish_lat"
    t.float "finish_long"
    t.string "start_address"
    t.string "finish_address"
    t.decimal "price"
    t.integer "split"
    t.datetime "start_hours"
    t.bigint "modality_id"
    t.string "photo"
    t.index ["modality_id"], name: "index_events_on_modality_id"
    t.index ["organization_id"], name: "index_events_on_organization_id"
  end

  create_table "holder_images", force: :cascade do |t|
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photos"
    t.index ["event_id"], name: "index_holder_images_on_event_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "product_id"
    t.string "product_desc"
    t.decimal "price"
    t.string "quantity"
    t.string "customer_name"
    t.string "customer_address"
    t.bigint "athlete_id"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "discount"
    t.decimal "final_price"
    t.integer "dependent_id"
    t.boolean "processed", default: false
    t.string "transaction_id"
    t.index ["athlete_id"], name: "index_invoices_on_athlete_id"
    t.index ["event_id"], name: "index_invoices_on_event_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.bigint "athlete_id"
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "approved"
    t.boolean "recall_authorize"
    t.string "voucher"
    t.integer "status"
    t.index ["athlete_id"], name: "index_memberships_on_athlete_id"
    t.index ["event_id"], name: "index_memberships_on_event_id"
  end

  create_table "modalities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name"
    t.string "street"
    t.string "number"
    t.string "neighbor"
    t.string "city"
    t.string "state"
    t.string "cnpj"
    t.string "cpf"
    t.string "email"
    t.string "phone"
    t.string "second_phone"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "bank_code"
    t.string "bank_agency"
    t.string "bank_agency_dv"
    t.string "bank_account"
    t.string "bank_account_dv"
    t.string "bank_account_type"
    t.string "bank_document"
    t.string "bank_account_name"
    t.string "account_id"
    t.string "recipent_id"
    t.string "photo"
  end

  create_table "responses", force: :cascade do |t|
    t.string "response"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teams", force: :cascade do |t|
    t.string "team_name"
    t.string "mote"
    t.string "team_image"
    t.string "state"
    t.string "mode"
    t.text "team_info"
    t.bigint "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "visibility"
    t.index ["owner_id"], name: "index_teams_on_owner_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "vouchers", force: :cascade do |t|
    t.string "number"
    t.boolean "used", default: false
    t.bigint "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "discount"
    t.index ["event_id"], name: "index_vouchers_on_event_id"
  end

  add_foreign_key "athlete_teams", "athletes"
  add_foreign_key "athlete_teams", "teams"
  add_foreign_key "checkouts", "invoices"
  add_foreign_key "comemberships", "dependents"
  add_foreign_key "comemberships", "events"
  add_foreign_key "dependents", "athletes"
  add_foreign_key "events", "modalities"
  add_foreign_key "events", "organizations"
  add_foreign_key "holder_images", "events"
  add_foreign_key "invoices", "athletes"
  add_foreign_key "invoices", "events"
  add_foreign_key "memberships", "athletes"
  add_foreign_key "memberships", "events"
  add_foreign_key "orders", "users"
  add_foreign_key "teams", "athletes", column: "owner_id"
  add_foreign_key "vouchers", "events"
end
