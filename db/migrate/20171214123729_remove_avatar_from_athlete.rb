class RemoveAvatarFromAthlete < ActiveRecord::Migration[5.1]
  def change
    remove_column :athletes, :avatar
  end
end
