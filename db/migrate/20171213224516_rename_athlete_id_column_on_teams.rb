class RenameAthleteIdColumnOnTeams < ActiveRecord::Migration[5.1]
  def change
    rename_column :teams, :athlete_id, :owner_id
  end
end
