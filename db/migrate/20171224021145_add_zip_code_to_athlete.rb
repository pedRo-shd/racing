class AddZipCodeToAthlete < ActiveRecord::Migration[5.1]
  def change
    add_column :athletes, :zipcode, :string
  end
end
