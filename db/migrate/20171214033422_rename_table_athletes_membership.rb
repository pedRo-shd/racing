class RenameTableAthletesMembership < ActiveRecord::Migration[5.1]
  def change
    rename_table :athletes_memberships, :memberships
  end
end
