class CreateAthleteTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :athlete_teams do |t|
      t.references :athlete, foreign_key: true
      t.references :team, foreign_key: true

      t.timestamps
    end
  end
end
