class AddPhotoToModalities < ActiveRecord::Migration[5.1]
  def change
    add_column :modalities, :photo, :string
  end
end
