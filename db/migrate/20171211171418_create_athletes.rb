class CreateAthletes < ActiveRecord::Migration[5.1]
  def change
    create_table :athletes do |t|
      t.string :name
      t.string :email
      t.string :gender
      t.string :cpf
      t.date   :birth_date
      t.string :avatar
      t.string :password_digest

      t.timestamps
    end
  end
end
