class CreateDependents < ActiveRecord::Migration[5.1]
  def change
    create_table :dependents do |t|
      t.string :name
      t.string :gender
      t.string :cpf
      t.date :birth_date
      t.references :athlete, foreign_key: true

      t.timestamps
    end
  end
end
