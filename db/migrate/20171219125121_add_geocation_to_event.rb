class AddGeocationToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :finish_street, :string
    add_column :events, :finish_number, :string
    add_column :events, :finish_neighbor, :string
    add_column :events, :finish_city, :string
    add_column :events, :finish_state, :string
    add_column :events, :start_lat, :float
    add_column :events, :start_long, :float
    add_column :events, :finish_lat, :float
    add_column :events, :finish_long, :float
  end
end
