class AddStatusToComembership < ActiveRecord::Migration[5.1]
  def change
    add_column :comemberships, :approved, :boolean, default: false
    add_column :comemberships, :status, :integer
  end
end
