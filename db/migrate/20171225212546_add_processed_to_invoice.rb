class AddProcessedToInvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :processed, :boolean, default: false
  end
end
