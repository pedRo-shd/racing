class AddModalityToEvents < ActiveRecord::Migration[5.1]
  def change
    add_reference :events, :modality, foreign_key: true
  end
end
