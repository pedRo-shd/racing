class AddBankFieldsToAdmin < ActiveRecord::Migration[5.1]
  def change
    add_column :admins, :bank_code, :string
    add_column :admins, :bank_agency, :string
    add_column :admins, :bank_agency_dv, :string
    add_column :admins, :bank_account, :string
    add_column :admins, :bank_account_dv, :string
    add_column :admins, :bank_account_name, :string
    add_column :admins, :bank_document, :string
    add_column :admins, :account_id, :string
    add_column :admins, :recipent_id, :string
  end
end
