class AddRecipentIdToOrganization < ActiveRecord::Migration[5.1]
  def change
    add_column :organizations, :recipent_id, :string
  end
end
