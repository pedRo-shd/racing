class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.integer :product_id
      t.string :product_desc
      t.decimal :price
      t.string :quantity
      t.string :customer_name
      t.string :customer_address
      t.references :athlete, foreign_key: true
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
