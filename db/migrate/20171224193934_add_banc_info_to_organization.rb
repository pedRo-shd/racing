class AddBancInfoToOrganization < ActiveRecord::Migration[5.1]
  def change
    add_column :organizations, :bank_code, :string
    add_column :organizations, :bank_agency, :string
    add_column :organizations, :bank_agency_dv, :string
    add_column :organizations, :bank_account, :string
    add_column :organizations, :bank_account_dv, :string
    add_column :organizations, :bank_account_type, :string
    add_column :organizations, :bank_document, :string
    add_column :organizations, :bank_account_name, :string
  end
end
