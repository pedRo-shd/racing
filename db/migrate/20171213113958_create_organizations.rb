class CreateOrganizations < ActiveRecord::Migration[5.1]
  def change
    create_table :organizations do |t|
      t.string :name
      t.string :street
      t.string :number
      t.string :neighbor
      t.string :city
      t.string :state
      t.string :cnpj
      t.string :cpf
      t.string :email
      t.string :phone
      t.string :second_phone
      t.string :password_digest

      t.timestamps
    end
  end
end
