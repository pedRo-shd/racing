class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :name
      t.string :street
      t.string :number
      t.string :neighbor
      t.string :city
      t.string :state
      t.date   :event_date
      t.text   :event_info
      t.string :event_image
      t.string :holders_image
      t.string :kit_image
      t.string :event_users_image
      t.string :contact_email
      t.string :contact_phone
      t.string :status
      t.string :mode
      t.text   :rules
      t.references :organization, foreign_key: true

      t.timestamps
    end
  end
end
