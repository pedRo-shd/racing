class AddAccountIdToOrganization < ActiveRecord::Migration[5.1]
  def change
    add_column :organizations, :account_id, :string
  end
end
