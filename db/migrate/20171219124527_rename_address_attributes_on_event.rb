class RenameAddressAttributesOnEvent < ActiveRecord::Migration[5.1]
  def change
    rename_column :events, :street, :start_street
    rename_column :events, :number, :start_number
    rename_column :events, :neighbor, :start_neighbor
    rename_column :events, :city, :start_city
    rename_column :events, :state, :start_state    
  end
end
