class AddPhotosToHolderImages < ActiveRecord::Migration[5.1]
  def change
    add_column :holder_images, :photos, :string
  end
end
