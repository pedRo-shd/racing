class AddColumnApprovedToAthlete < ActiveRecord::Migration[5.1]
  def change
    add_column :athletes, :approved, :boolean
  end
end
