class AddAttachmentAvatarToDependents < ActiveRecord::Migration[5.1]
  def self.up
    change_table :dependents do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :dependents, :avatar
  end
end
