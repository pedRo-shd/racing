class AddFieldsSubscriptionsToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :subscription_start, :date
    add_column :events, :subscription_end, :date
    add_column :events, :max_subscriptions, :integer
  end
end
