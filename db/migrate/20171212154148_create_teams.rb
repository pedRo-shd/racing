class CreateTeams < ActiveRecord::Migration[5.1]
  def change
    create_table :teams do |t|
      t.string :team_name
      t.string :mote
      t.string :team_image
      t.string :state
      t.string :mode
      t.text :team_info
      t.references :athlete, foreign_key: true

      t.timestamps
    end
  end
end
