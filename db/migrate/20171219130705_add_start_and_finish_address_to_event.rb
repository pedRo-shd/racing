class AddStartAndFinishAddressToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :start_address, :string
    add_column :events, :finish_address, :string
  end
end
