class AddStartHoursToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :start_hours, :datetime
  end
end
