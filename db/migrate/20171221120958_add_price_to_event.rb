class AddPriceToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :price, :decimal
  end
end
