class CreateComemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :comemberships do |t|
      t.references :dependent, foreign_key: true
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
