class AddRecallToMembership < ActiveRecord::Migration[5.1]
  def change
    add_column :memberships, :recall_authorize, :boolean, dafault: false
  end
end
