class AddAddressToAthlete < ActiveRecord::Migration[5.1]
  def change
    add_column :athletes, :street, :string
    add_column :athletes, :number, :string
    add_column :athletes, :neighbor, :string
    add_column :athletes, :city, :string
    add_column :athletes, :state, :string
  end
end
