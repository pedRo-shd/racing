class CreateAthletesMemberships < ActiveRecord::Migration[5.1]
  def change
    create_table :athletes_memberships do |t|
      t.references :athlete, foreign_key: true
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
