class RenameMewEventDate < ActiveRecord::Migration[5.1]
  def change
    rename_column :events, :mew_event_date, :new_event_date
  end
end
