class AddColumsToCheckout < ActiveRecord::Migration[5.1]
  def change
    add_reference :checkouts, :invoice, foreign_key: true
  end
end
