class AddVoucherToMembership < ActiveRecord::Migration[5.1]
  def change
    add_column :memberships, :voucher, :string
  end
end
