class AddBeRecalledToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :be_recalled, :boolean, default: false
  end
end
