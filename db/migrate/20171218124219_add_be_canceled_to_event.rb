class AddBeCanceledToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :be_canceled, :boolean
  end
end
