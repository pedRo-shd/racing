class AddApprovedFieldToMembership < ActiveRecord::Migration[5.1]
  def change
    add_column :memberships, :approved, :boolean
  end
end
