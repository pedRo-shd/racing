class AddDateOfRecalledEventToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :recall_start, :date
    add_column :events, :recall_end, :date
    add_column :events, :mew_event_date, :date
  end
end
