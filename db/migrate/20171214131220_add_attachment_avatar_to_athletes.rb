class AddAttachmentAvatarToAthletes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :athletes do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :athletes, :avatar
  end
end
