class CreateHolderImages < ActiveRecord::Migration[5.1]
  def change
    create_table :holder_images do |t|
      t.references :event, foreign_key: true

      t.timestamps
    end
  end
end
