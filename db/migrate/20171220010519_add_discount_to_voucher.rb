class AddDiscountToVoucher < ActiveRecord::Migration[5.1]
  def change
    add_column :vouchers, :discount, :integer
  end
end
