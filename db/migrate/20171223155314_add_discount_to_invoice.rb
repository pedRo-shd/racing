class AddDiscountToInvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :discount, :integer
  end
end
