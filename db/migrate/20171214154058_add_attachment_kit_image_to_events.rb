class AddAttachmentKitImageToEvents < ActiveRecord::Migration[5.1]
  def self.up
    change_table :events do |t|
      t.attachment :kit_image
    end
  end

  def self.down
    remove_attachment :events, :kit_image
  end
end
