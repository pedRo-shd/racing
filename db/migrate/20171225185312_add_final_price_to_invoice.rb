class AddFinalPriceToInvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :final_price, :decimal
  end
end
