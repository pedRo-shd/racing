class AddTransactionIdToInvoice < ActiveRecord::Migration[5.1]
  def change
    add_column :invoices, :transaction_id, :string
  end
end
