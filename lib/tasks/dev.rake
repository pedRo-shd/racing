namespace :dev do
   desc 'Create Modalities'
   task setup: :environment do

   puts 'Building modalities...'

   Modality.create!(
    name: 'BMX',
    photo: File.new(Rails.root.join('public','modalities', 'bmx.png'), 'r')

   )

   Modality.create!(
    name: 'Cicloturismo',
    photo: File.new(Rails.root.join('public','modalities', 'cicloturismo.png'), 'r')
   )

   Modality.create!(
    name: 'Duathlon',
    photo: File.new(Rails.root.join('public','modalities', 'duathlon.png'), 'r')
   )

   Modality.create!(
    name: 'MTB',
    photo: File.new(Rails.root.join('public','modalities', 'mtb.png'), 'r')
   )

   Modality.create!(
    name: 'Paraciclismo',
    photo: File.new(Rails.root.join('public','modalities', 'paraciclismo.png'), 'r')
   )

   Modality.create!(
    name: 'Speed',
    photo: File.new(Rails.root.join('public','modalities', 'speed.png'), 'r')
   )

   Modality.create!(
    name: 'Triathlon',
    photo: File.new(Rails.root.join('public','modalities', 'triathlon.png'), 'r')
   )

   puts 'Created photos from modalities with success!'

 end



end
