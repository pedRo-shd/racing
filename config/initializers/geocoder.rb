Geocoder.configure(
  timeout: 7,                 # geocoding service timeout (secs)
  lookup: :google,            # name of geocoding service (symbol)
)