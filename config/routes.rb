Rails.application.routes.draw do

  post 'contacts', to: 'contacts#create'

  get 'cities', to: 'cities#cities_br'
  get 'states', to: 'cities#states_br'
  get 'modalities', to: 'modalities#index'

  # Athletes auth routes
  post 'athlete_auth/login', to: 'authentication#athlete_authenticate'
  post 'athlete_signup', to: 'athletes#create'
  put 'athlete_edit', to: 'athletes#update'
  get 'athlete', to: 'athletes#athlete'
  get 'athlete_events', to: 'athletes#athlete_events'
  get 'to_be_recalled', to: 'athletes#to_be_recalled'
  get 'get_athlete/:id', to: 'athletes#get_athlete'
  put 'accept_recall', to: 'athletes#accept_recall'
  resources :athletes, only: %i[avatar_url reset_athlete_apssword] do
    member do
      get 'avatar_url'
    end
    collection do
      put 'reset_athlete_password'
      get 'subscription_to_pay'
    end
  end
  # Organization auth routes
  post 'organization_auth/login', to: 'authentication#organization_authenticate'
  post 'organization_signup', to: 'organizations#create'
  put 'organizations', to: 'organizations#update'
  get 'get_all_subscriptions', to: 'organizations#get_all_subscriptions'
  resources :organizations, only: %i[reset_athlete_apssword org_subscriptions_status org_co_subscriptions_status show organization] do
    collection do
      put 'reset_organization_password'
      get 'org_subscriptions_status'
      get 'org_co_subscriptions_status'
      get 'organization'
    end
  end
  # Admin auth routes
  post 'admin_auth/login', to: 'authentication#admin_authenticate'
  post 'admin_signup', to: 'admins#create'
  put 'admin_edit', to: 'admins#update'

  get 'search', to: 'admins#search'
  resources :admins, only: %i[reset_athlete_apssword to_be_canceled admin_cancel_event subscriptions_status co_subscriptions_status] do
    collection do
      put 'reset_admin_password'
      get 'to_be_canceled'
      put 'admin_cancel_event'
      get 'subscriptions_status'
      get 'co_subscriptions_status'
    end
  end
  # Admin returns
  get '/get_athletes', to: 'admins#get_athletes'
  get 'get_events', to: 'admins#get_events'
  get '/get_organizations', to: 'admins#get_organizations'
  get '/admin_to_be_recalled', to: 'admins#admin_to_be_recalled'

  # Namespace the controller without affect the URI
  scope module: :v1, constraints: ApiVersion.new('v1', true) do

    resources :next_events, only: %i[index]

    resources :dependents do
      collection do
        get 'load_teams'
        get 'load_events'
      end
      member do
        get 'member_team'
        get 'member_events'
        get 'avatar_url'
        put 'join_event'
        put 'leave_event'
      end
    end
    resources :teams do
      member do
        put 'join_team'
        put 'leave_team'
        put 'approve_join'
        get 'team_athletes'
      end
      collection do
        get 'pending_approval'
      end
    end

    get 'autocomplete', to: 'events#autocomplete'
    get 'organization_event/:id', to: 'events#organization_event'
    get 'day_to_go/:id', to: 'events#day_to_go'
    get 'hours_to_go/:id', to: 'events#hours_to_go'
    get 'minutes_to_go/:id', to: 'events#minutes_to_go'
    get 'get_events_carousel', to: 'events#get_events_carousel'
    resources :events do
      member do
        put 'join_event'
        put 'leave_event'
        get 'event_image_url'
        get 'kit_image_url'
        get 'holder_images'
        put 'clone_event'
        put 'recall_event'
        put 'cancel_event'
        put 'get_voucher', to: 'vouchers#create'
        put 'set_split', to: 'events/event_split'
      end
    end
    resources :holder_images, only: %i[create destroy]
    post 'checkout_bill', to: 'checkouts#checkout_bill'
    post 'checkout_card', to: 'checkouts#checkout_card'
    post 'response', to: 'checkouts#get_response'
    post 'bank_account', to: 'organizations#bank_account_create'
    post 'create_recipent', to: 'organizations#create_recipent'
    post 'response_update', to: 'checkouts#response_update'
  end
end
